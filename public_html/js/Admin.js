$(function () {
    var APPLICATION_ID = "502EC3B1-E848-3B5A-FF3C-0E6494463F00",
        SECRET_KEY = "21DC289A-F3BF-57FA-FFD6-E57B63540500",
        VERSION = "vl";
        
    backendless.initApp (APPLICATION_ID, SECRET_KEY, VERSION);
    
    var loginScript = $("login-template").html();
    var loginTemplate = handlebars.compile(loginScript);
    
    $('.main-container').html(loginTemplate);
    
    $(document).on('submit', '.form-signin', function(event){
        event.preventDefault();
        
        var data = $(this).serializeArray(),
        email = data[0].value,
        password = data[1].value;
        
     backendless.UserService.login(email, password, true,  new Backendless.Async(userLoggedIn, gotError))  
    });
});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
    
}

function userLoggedIn(user){
    console.log("user successfully logged in");
    
    var welcomeScript = $('#welcome-template').html();
}

function gotError(error){
    console.log("Error message - " + error.message);
    console.log("Error code - " + error.code); 
}