$(function () {
    var APPLICATION_ID = "502EC3B1-E848-3B5A-FF3C-0E6494463F00",
        SECRET_KEY = "21DC289A-F3BF-57FA-FFD6-E57B63540500",
        VERSION = "vl";
        
    backendless.initApp (APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);

    var wrapper = {
        posts: postsCollection.data
    };
    
    var blogScript = $("blogs-template").html();
    var blogTemplate = handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
    
}